---
layout: handbook-page-toc
title: "Reliability Engineering - How We Work: Issue Management and Prioritization Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

All requests for work to the Reliability Team come through the [Reliability Issue Tracker](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/new?issuable_template=default).  The management of this queue is an ongoing maintenance task for Reliability Engineers and Managers.  This page contains an overview of the criteria used in determining how work is triaged and prioritized.

## Issue Priority

Priority for incoming work is based on a matrix measuring the **impact** and **urgency** of an issue.

![priority_matrix](img/priority_matrix.png)

### Impact

Impact is the measure of the effect of an incident, problem, or change on business processes as detailed in the issue.

The table below can be used as a general guide for determining impact:

| Impact | Description |
| -------- | ----------- |
| High | -The issue needs to be resolved to mitigate an active S1 or S2 incident <br> -The issue is a roadblock on GitLab.com and blocking customer's business goals and day to day workflow <br> -The damage to the reputation of the business is likely to be high. <br> -Deploys are blocked as a result <br> -The potential financial impact is high|
| Medium | -The issue is impacting a moderate subset of employees or a small subset of customers <br> -The damage to the reputation of the business is not likely to be high.<br> - The potential financial impact is low but greater than 0 |
| Low | -The issue is impacting a small subset of employees <br> -There is no impact on customers <br> -There is no risk to the reputation of the business <br> -There is no financial impact|

### Urgency

Urgency is the speed at which an issues should be resolved based on business need or expectation.

The table below can be used as a general guide for determining impact:

| Urgency | Description |
| -------- | ----------- |
| High | -The impact increases rapidly over time. <br> -Damage to the reputation of the business will increase over time. <br> -Any roadblock that puts the guaranteed self-managed release date at risk <br> -A minor incident could be prevented from becoming a major incident by acting immediately. <br> -A member of senior leadership has requested urgency|
| Medium | -The impact increases only slightly over time. <br> -The damage to the reputation of the business will not increase over time. <br> -The customer has requested urgency.|
| Low | -The impact does not increase at all over time. <br> -The customer indicates that the issue is not urgent.|

### Priority

Once the impact and urgency of an issue has been determined, it is time to assign a priority.

The table below can be used as a general guide for assigning priority:

| Priority | Impact/Urgency | Action |
| -------- | ---------- | --------- |
| `~Reliability::P1` | Impact: High <br> Urgency: High | -An engineer is assigned as soon as possible.  <br> -Issue is handed off between regions until it is resolved or mitigated to the point that priority can be lowered.  <br> -Issues should be labeled as `~Reliability::P1` only when **immediate** action is required |
| `~Reliability::P2` |  Impact: High <br> Urgency: Medium <br> **or** <br> Impact: Medium <br> Urgency: High   | -Issue is prioritized above P3s and P4s <br> -Issue is worked on but is **not** handed off between regions <br> -If an engineer is changing roles before they are able to resolve the issue, it should be handed over/assigned to another engineer. |
| `~Reliability::P3` | Impact: High <br> Urgency: Low <br> **or** <br> Impact: Low Urgency: High <br> **or** <br> Impact: Medium Urgency: Medium | -Issue is prioritized above P4s only <br> -If an engineer is changing roles before they are able to resolve the issue, the issue should be dropped back into the tracker with a summary of what has been done so far and what the next steps are.|
| `~Reliability::P4` | Impact: Low <br> Urgency: Medium <br> **or** <br> Impact: Medium <br> Urgency: Low | -Engineers are not assigned until all higher priority work has been completed|
| `~Reliability::P5` | Impact: Low <br> Urgency: Low | -If an issue is determined to be a P5, there is a question on if the issue should be done at all.  If it turns out something was missed it can be moved to a higher priority.  Otherwise it should be closed with an explanatory note. |

**Note:** Issue priority levels generally align with the [severity levels](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#severities) defined within the Incident Management process.
### Service Level Agreements
The Reliability team uses the following response time SLAs for  all `~Reliability::P1` and `~Reliability::P2 issues` that also have the `~unblocks others` label.  The `~unblocks others` [label](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#other-labels) indicates that the request originated from outside of the [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/).

| Priority | Initial Response Time | Follow Up Response Time | Coverage | How to engage|
| -------- | ---------- | --------- | --------- | --------- |
| `~Reliability::P1` | 30 minutes | 4 hours | 24x7 | This priority level is reserved for issues requiring immediate attention.  To create a `~Reliability::P1` issue, first [declare an incident](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#reporting-an-incident) to page the [EOC](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#roles-and-responsibilities) |
| `~Reliability::P2` <br> and <br> `unblocks others` | 3 days | 7 days | 24x5 | Create an issue in the [Reliability Issue Tracker](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/) and use labels `~Reliability::P2` and `unblocks others`|

**Note**: Except for corrective actions and security issues, reliability does not use `~"priority"` or `~"severity"` labels, if these labels are added they will be removed.
{: .alert .alert-info}

## Classifying work

### Corrective Actions and Security issues

Corrective actions and security issues labeled as `~"corrective action"` or `~"security"` are labeled differently in the Reliability issue tracker.
For these issues, `severity::*` labels are set to meet specific SLOs.

- For security issue see the [Time to resolve table by severity](/handbook/security/#severity-and-priority-labels-on-security-issues)
- Corrective Actions SLOs are currently based on definitions from [Quality](/handbook/engineering/quality/issue-triage/#severity-slos):

| Corrective Action Label | SLO (days after issue has been created) |
| ------ | ------ |
| severity::1 | 1 week |
| severity::2 | 30 days |
| severity::3 | 60 days |
| severity::4 | 90 days |

**Note**: For issues labeled with `~"security"` and `~"corrective action"` we do not use the `~"priority::*"` labels, if added they will be removed. Please use the `Reliability::P*` labels instead.
{: .alert .alert-info}

### Labels

#### Workflow Labels

Issues should always fall in to one of the following states, as defined by the following labels:

1. `workflow-infra::Triage` - Applied to all new Reliability issues automatically.  It indicates that the issue has not yet been reviewed by the team.
2. `workflow-infra::Proposal` - Applied to any issues that require discussion, input or review.  Some examples include:
  - [Readiness Reviews](https://about.gitlab.com/handbook/engineering/infrastructure/production/readiness/)
  - Issues that exist specifically to document a conversation
   - Example: [Discussion: Should we recreate our zonal/regional clusters differently](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15192)
  - Issues that discuss possible courses of action and are looking to solicit input from the greater Reliability team.
3. `workflow-infra::Ready` - Applied after the issues has been triaged by an SRE or Engineering Manager within the Reliability Team.  Ensure the following questions are answered and labeled before marking an issue as `workflow-infra::Ready`:
  - What is the priority of the issue as defined by the [prioritization matrix](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#issue-priority)?
      - `~Reliability::P1`, `~Reliability::P2`, `~Reliability::P3`, `~Reliability::P4`, or `~Reliability::P5` based on the prioritization matrix.
4. `workflow-infra::In Progress` - This label should be applied _only_ after the issue has a Reliability Team member as an assignee.  This label is meant to represent only work that is actively in progress and not to indicate that an issue will be worked on in the future.
5. `workflow-infra::Done` - This label is applied only when the issue has been closed.

#### Service Labels

`service::` labels are used to route issues to the right team within Reliability.  All issues should have a `service::` label specified during the triage process.  If multiple services are involved choose the service that best fits based on the details in the issue.

##### Examples
| Service | Team | Issue |
| ------ | ------ | ------ |
| `service::Terraform`| [Foundations](/handbook/engineering/infrastructure/team/reliability/#foundations) | [Issue#17010](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/17010) |
| `service::Prometheus` | [Observability](/handbook/engineering/infrastructure/team/reliability/#observability) | [Issue#14574](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/14574)|
| `service::Gitaly` | [Gitaly Stable Counterpart](/handbook/engineering/infrastructure/team/stable-counterpart.html) | [Issue#16271](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16271) |
| `service::Sidekiq` | [General](/handbook/engineering/infrastructure/team/reliability/#general) | [Issue#15720](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/15720) |

#### Team Labels
During the traige process, issues will be routed to a [Reliability Team](/handbook/engineering/infrastructure/team/reliability/#reliability-teams) where appropriate.  See the table below for the current list of active team Labels:

| Label | Team |
| -------- | --------- |
| `~team::Foundations` | [Foundations](/handbook/engineering/infrastructure/team/reliability/foundations.html) |
| `~team::Observability` | [Observability](/handbook/engineering/infrastructure/team/reliability/observability.html) |
| `~team::Database Reliability` | [Database Reliability](/handbook/engineering/infrastructure/team/reliability/database-reliability.html) |
| `~team::Practices` | [Practices](/handbook/engineering/infrastructure/team/reliability/practices.html) |
| `~team::General` | [General](/handbook/engineering/infrastructure/team/reliability/general.html) |

#### T-shirt Labels
T-shirt labels are used to estimate the size of issues.  These are always a rough estimate and often need to be adjusted once the full scope of an issue is defined.

| Label | Estimated Time Requirement | Example |
| -------- | --------- | --------- |
| `tShirt-size::XS` | 4 hours or less | TBD |
| `tShirt-size::S` | 1 day or less| TBD |
| `tShirt-size::M` | 1 week or less | TBD |
| `tShirt-size::L` | 1 month or less | TBD |
| `tShirt-size::XL` | More than 1 month | TBD |

#### Other Labels

 - `unblocks others` - Apply this label to all issues that originate outside of the [Infrastructure Department](https://about.gitlab.com/handbook/engineering/infrastructure/)

## Issue Review

The [issue board for Reliability](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/3993753) is reviewed twice a week by Reliability Leadership Team.  If you have an urgent issue that you believe should be prioritized ahead of other work, please reach out to any Engineering Manager on the Reliability Team to discuss.

## Issue Triage

Issue Triage is performed twice weekly by members of the Reliability Leadership Team.  The process consists of four parts.

### 1. General Triage

1. Locate issues that need to be triaged using [this view](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues?sort=updated_desc&state=opened&label_name[]=workflow-infra::Triage) which shows `workflow-infra::Triage` tickets lists most recently updated issues first.
1. Choose the first issue in the list and ask the following questions:
    1. For issues where the team ownership of the involved service is clear, choose a [Reliability Team](/handbook/engineering/infrastructure/team/reliability/#reliability-teams) to assign the issue to and apply the appropriate [team label](/handbook/engineering/infrastructure/team/reliability/issues.html#team-labels).
    1. What is the priority of the issue?  Read the issue and assign a priority as defined by [our process](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#issue-priority).  Once priority has been determined, add the appropriate [priority label](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#priority).  NOTE: Prioritized issues are reviewed by Reliability Leadership multiple times a week, so it is OK if an issue is prioritized incorrectly.
1. Add the `workflow-infra::Ready` label.  This indicates that the issue has been through the prioritization process and is ready to be looked at by an engineer.
1. Return the `workflow-infra::Triage` [list view](https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues?sort=updated_desc&state=opened&label_name[]=workflow-infra::Triage), select the next issue in the list and put it through the same process.
1. Add a `service::` label.

### 2. General Issue Prioritization
The overall backlog of general issues is reviewed to assess and adjust [priority](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#issue-priority).  The following boards are used to help with this process:

1. [Overall Priority of **all** triaged Reliability Issues](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/3993753).
1. [All unassigned issues by priority](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/3993753?assignee_id=None)
1. [All issues in progress](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/3993753?label_name%5B%5D=workflow-infra%3A%3AIn%20Progress) - review to ensure all issues with the `~workflow-infra::In Progress` are truly active.
1. [All assigned issues by priority](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/3993753?assignee_id=Any)

#### General Prioritization Guidelines
 - Issues should be prioritized by their priority label first
 - When faced with  multiple issues of the same priority level:
   - prioritize small issues ahead of large ones.
   - prioritize older issues over new.

### 3. Corrective Action Issue Review and Prioritization
[Corrective Actions](https://about.gitlab.com/handbook/engineering/infrastructure/incident-management/#corrective-actions) for all of Infrastructure are reviewed, prioritized, and assigned to prevent or reduce the likelihood and/or impact of an incident recurrence.

1. [All Open Infrastructure Corrective Actions by Severity](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/1181205?label_name%5B%5D=corrective%20action)
1. [All Open Reliability Corrective Actions by Priority](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/5066878?label_name[]=corrective%20action)
   1. [Observability](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/5066878?label_name[]=corrective%20action&label_name[]=Squad%3A%3AObservability)
   1. [Foundations](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/5066878?label_name%5B%5D=corrective%20action&label_name%5B%5D=Squad%3A%3AFoundations)
   1. [Gitaly Stable Counterpart](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/5066878?label_name%5B%5D=corrective%20action&label_name%5B%5D=Squad%3A%3AGitaly-StableCounterpart)
   1. [CI Runners Stable Counterpart](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/5066878?label_name[]=corrective%20action&label_name[]=Squad%3A%3ACI-RunnersStableCounterpart)
   1. [Datastores::Postgres](https://gitlab.com/gitlab-com/gl-infra/reliability/-/boards/5066878?label_name[]=corrective%20action&label_name[]=Squad%3A%3ADatabase)

### 4. External Issue Review

1. [All Active Incident Issues](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Incident%3A%3AActive&first_page_size=100)
2. [Production Change Requests](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=change%3A%3Aunscheduled&first_page_size=100)

## Capacity Planning Warnings
Capacity planning warning issues are generated by the Scalability team and indicate concerns that we should address before they cause an alert (or an outage!).  These issues are tracked outside of the [Reliability project](https://gitlab.com/gitlab-com/gl-infra/reliability) in the [capacity planning project](https://gitlab.com/gitlab-com/gl-infra/capacity-planning/).  The list of issues having the ~team::Reliability label is reviewed once per quarter and the work is then assigned based on impact and urgency.

 * [Current Reliability Capacity Planning Warning Issues](https://gitlab.com/gitlab-com/gl-infra/capacity-planning/-/issues/?label_name%5B%5D=team%3A%3AReliability)
