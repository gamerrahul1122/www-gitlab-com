---
title: Initial announcement
description: "Initial announcement and discussion on Hacker News"
canonical_path: "/blog/2012/08/24/initial-announcement/"
tags: untagged
categories: company
status: publish
type: post
published: true
meta:
  _elasticsearch_indexed_on: '2012-08-24 18:34:26'
---
Initial announcement and discussion [on Hacker News](http://news.ycombinator.com/item?id=4428278).