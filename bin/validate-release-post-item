#!/usr/bin/env ruby
#
# Validate release post items conform to the proper format
#
# Examples:
#   bin/validate-release-post-item
#   bin/validate-release-post-item [filename/dir]

require 'yaml'
require 'json'
require 'json_schemer'
require 'pathname'

STAGES_PATH = File.expand_path('../data/stages.yml', __dir__)
CATEGORIES_PATH = File.expand_path('../data/categories.yml', __dir__)

module ReleasePostHelpers
  Abort = Class.new(StandardError)
  Exception = Class.new(StandardError)
  Done = Class.new(StandardError)

  def fail_with(message)
    raise Exception, "\e[31merror\e[0m #{message}"
  end

  def abort_with(message)
    raise Abort, "\e[31merror\e[0m #{message}"
  end

  def warn_with(message)
    puts "\e[33mwarning\e[0m #{message}"
  end

  def decode_json_schemer_errors(error)
    data_path = format_data_pointer error['data_pointer']

    case error['type']
    when 'required'
      keys = error['details']['missing_keys'].join(', ')
      "#{data_path} is missing required keys: #{keys}"
    when 'null',
         'string',
         'boolean',
         'integer',
         'number',
         'array',
         'object'
      "property '#{data_path}' should be of type: #{error['type']}"
    when 'pattern'
      pattern = error['schema']['pattern']
      "property '#{data_path}' does not match pattern: #{pattern}"
    when 'format'
      format = error['schema']['format']
      "property '#{data_path}' does not match format: #{format}"
    when 'enum'
      options = error['schema']['enum']
      "property '#{data_path}' is not one of enum: #{options}"
    else
      "does not validate: error_type=#{error['type']}"
    end
  end

  def format_data_pointer(data_pointer)
    return 'root' if data_pointer.nil? || data_pointer.empty?

    data_pointer
      .sub(%r{^/}, '')   # remove leading /
      .sub('/', '.')     # convert / into .
  end
end

class ReleasePostEntry
  include ReleasePostHelpers

  def initialize(file_path, schema)
    @file_path = file_path
    @schema = schema

    unless File.fnmatch?("**.{yaml,yml}", file_path, File::FNM_EXTGLOB)
      fail_with "#{file_path} does not have a valid YAML extension"
    end

    f = File.open(file_path).read
    begin
      @item = YAML.safe_load(f)
    rescue StandardError
      fail_with "#{file_path}: invalid YAML"
    end
  end

  def execute
    assert_validates_schema

    if feature?
      assert_stage_key feature['stage']

      names = Array(feature['categories'])
      names.each { |n| assert_category_name n }

      assert_image
    end

    puts "\e[32mpass\e[0m #{@file_path}"
  end

  def feature?
    @item.has_key? 'features'
  end

  def feature
    (@item['features']['top'] || @item['features']['primary'] || @item['features']['secondary'])[0]
  end

  def release
    Pathname(@file_path).each_filename.to_a[-2]
  end

  def assert_validates_schema
    return if @schema.valid?(@item)

    errors = @schema.validate(@item).to_a
    messages = errors.map { |err| decode_json_schemer_errors err }

    fail_with "#{@file_path}: #{messages}"
  end

  def assert_stage_key(key)
    return if key.nil?

    stages = YAML.load_file(STAGES_PATH)['stages'].map { |key, value| [key, value['display_name']] }.to_h
    stage_keys = stages.keys

    return if stage_keys.include? key

    message = "#{@file_path}: invalid stage, key '#{key}' not found in #{STAGES_PATH}"

    # Check for common mistakes
    suggested_key = key.downcase.tr(' ', '_') if stage_keys.include?(key || '').downcase.tr(' ', '_')
    message << "\n  Did you mean '#{suggested_key}'?" if suggested_key

    fail_with message
  end

  def assert_category_name(name)
    return if name.nil?

    categories = YAML.load_file(CATEGORIES_PATH).map { |key, value| [key, value['name']] }.to_h
    category_names = categories.values

    return if category_names.include? name

    message = "#{@file_path}: invalid category, name '#{name}' name not found in #{CATEGORIES_PATH}"

    # Check for common mistakes
    suggested_category =
      categories[name] ||
      categories[name.downcase.tr('-', '_')] ||
      categories[name.downcase.tr(' ', '_')]
    message << "\n  Did you mean '#{suggested_category}'?" if suggested_category

    fail_with message
  end

  def assert_image
    return unless feature['image_url']

    unless feature['image_url'].start_with?("/images/#{release}/")
      fail_with "#{@file_path}: image_url should start with /images/#{release}/" 
    end

    img = File.join('source', feature['image_url'])
    fail_with "#{@file_path}: image #{img} not found" unless File.exist?(img)

    max_size = 150 * 1000
    size = File.size(img)
    return unless size > max_size

    fail_with "#{@file_path}: image #{img} is too large #{size}B. Max size is #{max_size}B."
  end
end

def items
  search_path = ARGV.shift || 'data/release_posts/unreleased'

  return [search_path] if File.file?(search_path)

  unless File.directory?(search_path)
    abort_with "#{search_path} does not exist."
  end

  Dir.glob(File.join(search_path, '*')).reject { |f| File.directory?(f) }
end

if $PROGRAM_NAME == __FILE__
  exit_code = 0

  schema = JSONSchemer.schema(Pathname.new(File.expand_path('../data/schemas/releasepost.schema.json', __dir__)))

  items.each do |i|
    begin
      ReleasePostEntry.new(i, schema).execute
    rescue ReleasePostHelpers::Exception => ex
      puts ex.message
      exit_code = 1
    end
  end

  exit exit_code
end
